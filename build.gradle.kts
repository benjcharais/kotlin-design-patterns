plugins {
    kotlin("jvm") version "1.6.10"
}

group = "com.trulioo"
version = "0.1"

repositories {
    mavenCentral()
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    dependencies {
        implementation(kotlin("stdlib"))

        testImplementation(kotlin("test"))
    }
}
package com.trulioo.creational

import org.junit.jupiter.api.Test
import kotlin.test.BeforeTest
import kotlin.test.assertTrue

internal class SingletonTest {

    private val kotlinSingleton = KotlinSingleton        // Kotlin Singleton
    private val mySingleton = SingleLogger.getInstance() // Singleton class
    private val myLogger = MyLogger()                    // Standard class for comparison

    @BeforeTest
    fun before() {
        println("")
    }

    @Test
    fun validateStandardClassDifferentNamePerInstance() {
        myLogger.log("Hello from the standard class!")

        val newReference = MyLogger()
        newReference.log("Hello from my new instance!")

        // !== checks for NOT the same Object
        // !=  checks for NOT the same value
        assertTrue { myLogger !== newReference }
        assertTrue { myLogger.loggerNumber != newReference.loggerNumber }
    }

    @Test
    fun validateKotlinObjectIsSame() {
        kotlinSingleton.log("Hello from KotlinSingleton")

        val newReference = KotlinSingleton
        newReference.log("Hello from New KotlinSingleton Reference")

        // Triple '=' checks for same Object
        // Double '=' checks for same value
        assertTrue { kotlinSingleton === newReference }
        assertTrue { kotlinSingleton.loggerNumber == newReference.loggerNumber }
    }

    @Test
    fun validateCustomSingletonIsSame() {
        mySingleton.log("Hello from Custom Singleton!")

        val newReference = SingleLogger.getInstance()
        newReference.log("Hello from New Custom Singleton Reference")

        assertTrue { mySingleton === newReference }
        assertTrue { mySingleton.loggerNumber == newReference.loggerNumber }
    }
}
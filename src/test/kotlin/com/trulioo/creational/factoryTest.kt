package com.trulioo.creational

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class FactoryTest {

    private lateinit var factory: EncryptionFactory
    private lateinit var message: String

    @BeforeTest
    fun before() {
        factory = EncryptionFactory()
        message = "Jack's Quacks are the best around"

        println("")
    }

    @Test
    fun rotEncryptIsNotMessage() {
        println("Testing: rot")
        val encrypted = getFactoryEncrypted("rot")

        assertTrue { message != encrypted }
    }

    @Test
    fun rotDecryptIsMessage() {
        println("Testing: rot")
        val decrypted = getFactoryDecrypted("rot")

        assertTrue { message == decrypted }
    }

    @Test
    fun xorEncryptIsNotMessage() {
        println("Testing: xor")
        val encrypted = getFactoryEncrypted("xor")

        assertTrue { message != encrypted }
    }

    @Test
    fun xorDecryptIsMessage() {
        println("Testing: xor")
        val decrypted = getFactoryDecrypted("xor")

        assertTrue { message == decrypted }
    }

    private fun getFactoryEncrypted(factoryType: String): String {
        val encryptor = factory.create(factoryType)

        println("Original message: $message")
        val encrypted = encryptor.encrypt(message)
        println("""Encrypted message: $encrypted""".trimMargin())

        return encrypted
    }

    private fun getFactoryDecrypted(factoryType: String): String {
        val encrypted = getFactoryEncrypted(factoryType)

        val decrypted = factory.create(factoryType).decrypt(encrypted)

        println("Decrypted: $decrypted")

        return decrypted
    }
}
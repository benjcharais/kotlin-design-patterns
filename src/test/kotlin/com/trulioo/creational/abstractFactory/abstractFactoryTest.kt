package com.trulioo.creational.abstractFactory

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class AbstractFactoryTest {

    private lateinit var dessertFactory: ConfectioneryFactory

    @BeforeTest
    fun before() {
        dessertFactory = ConfectioneryFactory()
    }

    @Test
    fun factoryBuildsNewInstance() {
        val first = dessertFactory.buildPastryShop("Schrodinger's Pastries")
        val second = dessertFactory.buildPastryShop("Schrodinger's Pastries")

        assertTrue { first !== second }
    }

    @Test
    fun shopInstanceProducesCorrectConfectionary() {
        val shop = dessertFactory.buildDonutShop("Carl's Confectionary Emporium")

        val eclaire = shop.bake(Donuts.Eclaire)

        println("Our eclaire from '${shop.name}' is filled with ${eclaire.filling.joinToString()}")

        assertTrue { eclaire is Eclaire }
    }

    @Test
    fun canProduceMultiple() {
        val pastries = dessertFactory.buildPastryShop(PASTRY_SHOP)
        val cakes = dessertFactory.buildCakeShop(CAKE_SHOP)
        val donuts = dessertFactory.buildDonutShop(DONUT_SHOP)

        val groupOrder = mutableListOf<IConfection>()

        with(groupOrder) {
            add(pastries.bake(Pastries.AppleFritter))
            add(pastries.bake(Pastries.Sfogliatelle))
            add(pastries.bake(Pastries.StrawberryDanish))

            add(cakes.bake(Cakes.Cheese))
            add(cakes.bake(Cakes.Carrot))

            for (i in 1..13) add(donuts.bake(Donuts.OldFashioned))
            for (i in 1..13) add(donuts.bake(Donuts.Eclaire))
            for (i in 1..13) add(donuts.bake(Donuts.Jelly))
        }

        val donutOrder = mutableListOf<IDonut>()

        println("We got... ")
        groupOrder.forEach {
            if (it is IDonut) donutOrder.add(it)
            else explainItem(it)
        }

        explainDonuts(donutOrder)

        println("Everything came from ${pastries.name}, ${cakes.name}, and of course... ${donuts.name}")
    }

    private fun explainItem(item: IConfection) {
        println("  ${item::class.simpleName} filled with ${item.filling.joinToString()}, and topped with ${item.toppings.joinToString()}...")
    }

    private fun explainDonuts(donuts: List<IDonut>) {
        println("We also got ${donuts.size / 13} dozen donuts for all to share")
    }

    companion object {
        const val PASTRY_SHOP = "Marie Curie's Pastries"
        const val CAKE_SHOP = "Cake me home Country Road"
        const val DONUT_SHOP = "Donuts, Danishes, and Dancing"
    }
}
package com.trulioo.creational

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class StaticFactoryTest {

    private lateinit var cardDeck: CardDeck

    @BeforeTest
    fun before() {
        cardDeck = CardDeck.newDeck()
    }

    @Test
    fun takingCardRemovesFromDeck() {
        val previousSize = cardDeck.deck.size
        val card = cardDeck.takeCard()
        val newSize = cardDeck.deck.size

        println("We pulled the: ${card.value} of ${card.suit}")

        assertTrue { previousSize != newSize }
    }

    @Test
    fun checkCardExists() {
        val pulledCard = CardDeck.valueOf(CardSuit.SPADES, CardValue.ACE)

        assertTrue { CardSuit.SPADES == pulledCard.suit }
        assertTrue { CardValue.ACE == pulledCard.value }
    }

    @Test
    fun shufflingHasNewOrder() {
        val originalOrder = cardDeck.deck
        val newOrder = cardDeck.shuffled()

        println("The first 5 cards in our deck are: ")
        originalOrder.take(5).forEach {
            println("  The ${it.value} of ${it.suit}")
        }

        println("\nThe first 5 after shuffling are: ")
        newOrder.take(5).forEach {
            println("  The ${it.value} of ${it.suit}")
        }

        assertNotEquals(originalOrder, newOrder)
    }
}
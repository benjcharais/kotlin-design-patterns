package com.trulioo.creational.abstractFactory

class ConfectioneryFactory {
    private val _bakeries = mutableListOf<IBakery<*, IConfection>>()

    val bakeries get() = _bakeries.toList()

    fun buildPastryShop(name: String) : PastryShop {
        val shop = PastryShop(name)
        _bakeries.add(shop)
        return shop
    }

    fun buildCakeShop(name: String) : CakeShop {
        val shop = CakeShop(name)
        _bakeries.add(shop)
        return shop
    }

    fun buildDonutShop(name: String) : DonutShop {
        val shop = DonutShop(name)
        _bakeries.add(shop)
        return shop
    }
}


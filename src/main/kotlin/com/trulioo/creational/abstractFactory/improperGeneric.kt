package com.trulioo.creational.abstractFactory

/** Commented out to be compilable
class CalzoneZone(override val name: String) : IBakery<Pizza, IPizza> {
    override fun bake(type: Pizza): IPizza {
        return object : IPizza {}
    }
}
*/

enum class Pizza
interface IPizza : IBake

class DanishDelights(override val name: String) : IBakery<Donuts, IDanish> {
    override fun bake(type: Donuts): IDanish {
        TODO()
    }
}

interface IDanish : IConfection
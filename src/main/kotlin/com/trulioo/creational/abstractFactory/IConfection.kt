package com.trulioo.creational.abstractFactory

class AppleFritter : IPastry {
    override val crust: Boolean = true
    override val filling = listOf("Apple")
    override val toppings = listOf("Sugar")
}

class Sfogliatelle : IPastry {
    override val crust: Boolean = false
    override val filling = listOf("Orange Ricotta", "Almond", "Candied Citron")
    override val toppings = listOf("Confectioners Sugar")
}

class StrawberryDanish : IPastry {
    override val crust: Boolean = false
    override val filling = listOf("Strawberry Jam", "Cream Cheese")
    override val toppings = listOf("Crushed Almonds")
}

class CheeseCake : ICake {
    override val crust: Boolean = true
    override val filling = listOf("Cream Cheese")
    override val toppings = listOf("Chocolate Drizzle")
}

class CoffeeCake : ICake {
    override val crust: Boolean = true
    override val filling = listOf("Chocolate Crumbles", "Cinnamon Clusters")
    override val toppings = listOf("Powdered Sugar")
}

class CarrotCake : ICake {
    override val crust: Boolean = true
    override val filling = listOf("Raisins")
    override val toppings = listOf("Frosting")
}

class OldFashionedDonut : IDonut {
    override val crust: Boolean = true
    override val filling = listOf("None")
    override val toppings = listOf("Sugar Glaze")
}

class JellyDonut : IDonut {
    override val crust: Boolean = false
    override val filling = listOf("Strawberry Jelly")
    override val toppings = listOf("Chocolate")
}

class Eclaire : IDonut {
    override val crust: Boolean = false
    override val filling = listOf("Chopped Almonds", "Honey")
    override val toppings = listOf("Crushed Almonds")
}

interface IDonut : IConfection
interface IPastry : IConfection
interface ICake : IConfection

interface IConfection : IBake {
    val crust: Boolean
    val filling: List<String>
    val toppings: List<String>
}

interface IBake
package com.trulioo.creational.abstractFactory

enum class Pastries {
    AppleFritter, Sfogliatelle, StrawberryDanish
}

enum class Cakes {
    Cheese, Carrot, Coffee
}

enum class Donuts {
    OldFashioned, Jelly, Eclaire
}
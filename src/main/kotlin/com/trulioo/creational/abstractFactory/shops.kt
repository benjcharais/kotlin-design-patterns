package com.trulioo.creational.abstractFactory

class PastryShop(override val name: String) : IBakery<Pastries, IPastry> {
    override fun bake(type: Pastries): IPastry {
        return when (type) {
            Pastries.AppleFritter -> AppleFritter()
            Pastries.Sfogliatelle -> Sfogliatelle()
            Pastries.StrawberryDanish -> StrawberryDanish()
        }
    }
}

class CakeShop(override val name: String) : IBakery<Cakes, ICake> {
    override fun bake(type: Cakes): ICake {
        return when (type) {
            Cakes.Carrot -> CarrotCake()
            Cakes.Cheese -> CheeseCake()
            Cakes.Coffee -> CoffeeCake()
        }
    }
}

class DonutShop(override val name: String) : IBakery<Donuts, IDonut> {
    override fun bake(type: Donuts): IDonut {
        return when (type) {
            Donuts.Jelly -> JellyDonut()
            Donuts.OldFashioned -> OldFashionedDonut()
            Donuts.Eclaire -> Eclaire()
        }
    }
}

interface IBakery<in BakeryType: Enum<*>, out ConfectionType: IConfection> {
    val name: String

    fun bake(type: BakeryType) : ConfectionType
}



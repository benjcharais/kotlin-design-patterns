package com.trulioo.creational

import kotlin.math.absoluteValue
import kotlin.random.Random

// Kotlin Keyword Singleton
object KotlinSingleton : IMyLogger {
    override val prefix: String = "Kotlin Logger"
    override val loggerNumber: Int = Random.nextInt().absoluteValue
}

// Object Expression
fun myComplicatedCode() {

    val objectExpression = object : IMyLogger {
        override val prefix: String = "Expression Logger"
        override val loggerNumber: Int = Random.nextInt().absoluteValue
    }
}

// Custom Singleton
class SingleLogger private constructor() : MyLogger() {
    override val prefix: String = "Single Logger"
    override val loggerNumber: Int = Random.nextInt().absoluteValue

    companion object {
        private lateinit var instance: SingleLogger

        fun getInstance(): SingleLogger {
            if (Companion::instance.isInitialized.not()) instance = SingleLogger()
            return instance
        }
    }
}

// Normal extensible class
open class MyLogger : IMyLogger {
    override val prefix: String = "Trulioo Logger"
    override val loggerNumber: Int = Random.nextInt().absoluteValue
}

// Example Interface
interface IMyLogger {
    val prefix: String
    val loggerNumber: Int

    fun log(msg: String) {
        println("${prefix}-${loggerNumber}: $msg")
    }
}

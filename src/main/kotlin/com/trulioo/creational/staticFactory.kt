package com.trulioo.creational

class CardDeck private constructor() {

    private var _deck: List<ICard>

    val deck: List<ICard> get() = _deck

    init {
        val tempDeck = mutableListOf<ICard>()
        CardSuit.values().forEach { cardSuit ->
            CardValue.values().forEach { cardValue ->
                tempDeck.add(object : ICard {
                    override val suit: CardSuit = cardSuit
                    override val value: CardValue = cardValue
                })
            }
        }

        _deck = tempDeck
    }

    fun shuffled(): List<ICard> {
        _deck = _deck.shuffled()
        return _deck
    }

    fun takeCard(): ICard {
        val card = _deck.random()
        _deck = _deck.minus(card)

        return card
    }

    companion object {
        fun newDeck() = CardDeck()

        fun valueOf(suit: CardSuit, value: CardValue) : ICard {
            return object : ICard {
                override val suit: CardSuit = suit
                override val value: CardValue = value
            }
        }
    }
}

enum class CardValue {
    ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
}

enum class CardSuit {
    HEARTS, CLUBS, SPADES, DIAMONDS
}

interface ICard {
    val suit: CardSuit
    val value: CardValue
}
package com.trulioo.creational

import kotlin.experimental.xor

class EncryptionFactory {

    /*** Super Secret Key - Do not Share */
    private val encryptionKey: String = "Pineapple goes on pizza"

    fun create(encryptionType: String): IEncryptor {

        return when (encryptionType) {
            /*** Never actually use keys in this manner :) */
            "rot" -> RotEncrypt(encryptionKey)
            "xor" -> XorEncrypt(encryptionKey)
            else -> throw Exception("I'm sorry, Dave. I'm afraid I can't do that")
        }
    }
}

class RotEncrypt(override val key: String) : IEncryptor {
    private val rotValue = key.reduce { acc, c -> acc + c.code }

    override fun encrypt(message: String): String {
        return message.map { char ->
            char.plus(rotValue.code)
        }.joinToString("")
    }

    override fun decrypt(encrypted: String): String {
        return encrypted.map { char ->
            char.minus(rotValue.code)
        }.joinToString("")
    }
}

class XorEncrypt(override val key: String) : IEncryptor {
    override fun encrypt(message: String): String {
        return message
            .encodeToByteArray()
            .mapIndexed { ind, byte ->
                val index = ind % key.length
                val section = key[index].code.toByte()

                (byte xor section).toInt().toChar()
            }.joinToString("")
    }

    override fun decrypt(encrypted: String): String {
        return encrypt(encrypted)
    }
}

interface IEncryptor {

    val key: String

    fun encrypt(message: String): String
    fun decrypt(encrypted: String): String
}